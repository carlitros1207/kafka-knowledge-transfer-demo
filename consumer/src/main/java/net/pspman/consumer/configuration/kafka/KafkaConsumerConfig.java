package net.pspman.consumer.configuration.kafka;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.listener.adapter.RecordFilterStrategy;

import lombok.AllArgsConstructor;

@EnableKafka
@Configuration
@AllArgsConstructor
public class KafkaConsumerConfig {

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String,Object> concurrentKafkaListenerContainerFactory(ConcurrentKafkaListenerContainerFactory<String,Object> concurrentKafkaListenerContainerFactory) {
		concurrentKafkaListenerContainerFactory.setAckDiscarded(true);
		return concurrentKafkaListenerContainerFactory;
	}

	@Bean
	public RecordFilterStrategy<String,Object> KafkaWorkgroupRecordFilterStrategy(){
		return new KafkaWorkgroupRecordFilterStrategy<>("test");
	}
}
