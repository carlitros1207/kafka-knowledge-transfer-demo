package net.pspman.consumer.configuration.kafka;

import java.nio.charset.StandardCharsets;

import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.header.Header;
import org.springframework.kafka.listener.adapter.RecordFilterStrategy;

public class KafkaWorkgroupRecordFilterStrategy<K,V> implements RecordFilterStrategy<K, V> {

	private static final String WORKGROUP_HEADER = "workgroup";

	private final String workgroupToMatch;

	public KafkaWorkgroupRecordFilterStrategy(String workgroupToMatch) {
		this.workgroupToMatch = workgroupToMatch;
	}

	@Override
	public boolean filter(ConsumerRecord<K, V> consumerRecord) {
		if (workgroupToMatch == null) {
			return false;
		}

		String workgroup = null;
		for(Header header : consumerRecord.headers()){
			if(WORKGROUP_HEADER.equals(header.key())){
				if(header.value() != null){
					workgroup = new String(header.value(), StandardCharsets.UTF_8).intern();
				}
				break;
			}
		}
		return !StringUtils.equals(workgroupToMatch,workgroup);
	}
}
