package net.pspman.consumer.configuration.mysql;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@ConditionalOnProperty("pspman.database.config.url")
@Configuration()
public class DatabaseConfig {

	@Bean
	@ConfigurationProperties(prefix = "pspman.database.config")
	public DatabaseConfiguration databaseConfiguration() {
		return new DatabaseConfiguration();
	}

}
