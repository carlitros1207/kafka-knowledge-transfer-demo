package net.pspman.consumer.configuration.mysql;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DatabaseConfiguration {

	private String url;

	private String username;

	private String password;

	private String driver = "com.mysql.cj.jdbc.Driver";
}
