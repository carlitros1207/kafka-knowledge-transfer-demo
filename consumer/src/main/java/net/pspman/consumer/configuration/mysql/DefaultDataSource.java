package net.pspman.consumer.configuration.mysql;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ConditionalOnProperty("pspman.database.config.url")
@EnableTransactionManagement
public class DefaultDataSource {

	private final DatabaseConfiguration dbConfig;

	public DefaultDataSource(DatabaseConfiguration dbConfig) {
		this.dbConfig = dbConfig;
	}

	@Bean
	public DataSource dataSource() {
		return DataSourceBuilder.create()
				.url(dbConfig.getUrl())
				.username(dbConfig.getUsername())
				.password(dbConfig.getPassword())
				.driverClassName(dbConfig.getDriver())
				.build();
	}
}
