package net.pspman.consumer.dao;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;

import net.pspman.consumer.entity.DevelopersEntity;

public interface DevelopersDao extends JpaRepository<DevelopersEntity, BigDecimal> {
}
