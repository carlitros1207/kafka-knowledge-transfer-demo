package net.pspman.consumer.entity;

import java.math.BigDecimal;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "developers")
public class DevelopersEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private BigDecimal id;

	@Column
	private String name;

	@Column
	private String hobby;

	@Column(name = "offset")
	private long offset;

	@Column
	private int part;
}
