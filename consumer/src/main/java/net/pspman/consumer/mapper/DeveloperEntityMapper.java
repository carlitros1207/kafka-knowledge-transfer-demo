package net.pspman.consumer.mapper;

import org.mapstruct.Mapper;

import net.pspman.consumer.entity.DevelopersEntity;
import net.pspman.core.entity.DevelopersDTO;

@Mapper(componentModel = "spring")
public interface DeveloperEntityMapper {
	DevelopersEntity developersEntityToDeveloperEntity(DevelopersDTO entity);
}
