package net.pspman.consumer.service;

import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;
import net.pspman.consumer.dao.DevelopersDao;
import net.pspman.consumer.entity.DevelopersEntity;

@Log4j2
@Service
public class DevelopersService {

	private final DevelopersDao developersDao;

	public DevelopersService(DevelopersDao developersDao) {
		this.developersDao = developersDao;
	}


	public DevelopersEntity save(DevelopersEntity test){
		DevelopersEntity saved = developersDao.save(test);
		return saved;
	}
}
