package net.pspman.consumer.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.pspman.consumer.entity.DevelopersEntity;
import net.pspman.consumer.mapper.DeveloperEntityMapper;
import net.pspman.core.entity.DevelopersDTO;


@Log4j2
@Service
@AllArgsConstructor
public class KafkaReader {

	private final DevelopersService developersService;

	private final DeveloperEntityMapper mapper;

	@KafkaListener(topics = "developers", filter = "KafkaWorkgroupRecordFilterStrategy")
	public void testListener(ConsumerRecord<String, DevelopersDTO> payload, Acknowledgment ack) {
		try{
			DevelopersDTO entity = payload.value();
			DevelopersEntity test = mapper.developersEntityToDeveloperEntity(entity);
			test.setPart(payload.partition());
			test.setOffset(payload.offset());
			test.setHobby(entity.getHobby());
			test.setName(entity.getName());
			log.debug("Processing Partition: {}, Offset: {}, message: {}",test.getPart(), test.getOffset(),test);
			developersService.save(test);
		}catch (Exception e){
			log.error(e);
		} finally {
			ack.acknowledge();
		}

	}}
