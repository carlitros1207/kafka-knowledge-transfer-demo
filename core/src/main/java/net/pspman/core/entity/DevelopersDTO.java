package net.pspman.core.entity;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class DevelopersDTO {
	private BigDecimal id;
	private String name;
	private String hobby;
	private long offset;
	private int part;
}
