package net.pspman.producer.configuration.kafka;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaConfig {

	@Bean
	@ConfigurationProperties(prefix = "pspman.kafka.config")
	public KafkaConfiguration kafkaConfiguration(){
		return new KafkaConfiguration();
	}
}
