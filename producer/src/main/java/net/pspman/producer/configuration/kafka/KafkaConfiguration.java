package net.pspman.producer.configuration.kafka;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KafkaConfiguration {

	private KafkaProducer producer;

	private KafkaConsumer consumer;

	private String workgroup;

	@Data
	@NoArgsConstructor
	public static class KafkaProducer{

		private String bootstrapServers;

		private String clientId;

	}

	@Data
	@NoArgsConstructor
	public static class KafkaConsumer {

		private String bootstrapServers;

		private String groupId;

		private String clientId;

	}

}
