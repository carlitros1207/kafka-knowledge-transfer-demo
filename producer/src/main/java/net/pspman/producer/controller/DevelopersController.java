package net.pspman.producer.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.pspman.core.entity.DevelopersDTO;
import net.pspman.producer.service.KafkaProducer;

@Slf4j
@RestController
@RequestMapping(value = "developers", headers = {"Accept=application/json,application/xml"}, produces = {
		"application/json", "application/xml"})
@AllArgsConstructor
public class DevelopersController {

	private final KafkaProducer<DevelopersDTO> kafkaProducer;

	@PostMapping(value = "")
	public ResponseEntity<?> createDeveloper (@RequestBody DevelopersDTO developersDTO){
		kafkaProducer.sendMessage("developers", developersDTO.getName(), developersDTO);

		return ResponseEntity
				.status(HttpStatus.OK)
				.body(developersDTO);
	}
}
