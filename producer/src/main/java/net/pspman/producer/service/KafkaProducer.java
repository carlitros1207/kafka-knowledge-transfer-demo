package net.pspman.producer.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class KafkaProducer <T> {

	private static final String WORKGROUP_HEADER = "workgroup";

	private final KafkaTemplate<String,Object> kafkaTemplate;

	public KafkaProducer(KafkaTemplate<String, Object> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}

	public void sendMessage(String topic, String key, T message) {

		for (int i = 0; i < 1; i++) {
			ProducerRecord<String,Object> record = new ProducerRecord<>(topic,key,message);
			addHeader(record,"test");
			try {
				kafkaTemplate.send(record).whenCompleteAsync((r,e) -> log.info("published message to topic: {}, partition: {}, offset: {}",
						r.getRecordMetadata().topic(),
						r.getRecordMetadata().partition(),
						r.getRecordMetadata().offset()));
			} catch (Exception e) {
				log.error("error");
			}
		}
	}

	private void addHeader(ProducerRecord<String, Object> record, String workgroup){
		if(StringUtils.isNotBlank(workgroup)){
			record.headers().add(WORKGROUP_HEADER,workgroup.getBytes());
		}
	}
}
